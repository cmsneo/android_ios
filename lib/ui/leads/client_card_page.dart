import 'package:android_ios/bloc/bloc.dart';
import 'package:android_ios/common/icon_assets.dart';
import 'package:android_ios/common/styles/color_palettes.dart';
import 'package:android_ios/ui/notifications_page.dart';
import 'package:android_ios/utils/app_constants.dart';
import 'package:android_ios/utils/app_localizations.dart';
import 'package:android_ios/utils/navigation.dart';
import 'package:android_ios/widgets/button/custom_button.dart';
import 'package:android_ios/widgets/button/custom_button_white.dart';
import 'package:android_ios/widgets/button/custom_button_white_small.dart';
import 'package:android_ios/widgets/toast/custom_toast.dart';
import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';

class ClientCardPage extends StatefulWidget {
  static const routeName = "/clientCardPage";

  const ClientCardPage({Key key}) : super(key: key);

  _ClientCardPage createState() => _ClientCardPage();
}

class _ClientCardPage extends State<ClientCardPage> {


  final List<String> date = <String>['07.07.2020', '06.07.2020', '08.07.2020', '09.07.2020', '23.07.2020'];
  final List<String> time = <String>['15:04', '11:04', '13:04', '14:04', '12:04'];
  final List<String> commentedName = <String>['Айданова Айдана ', 'Айданова Айдана ', 'Айданова Айдана ', 'Айданова Айдана ', 'Айданова Айдана '];
  final List<String> comments = <String>['Хочет поговорить с учителем, чтобы понять программу, лучше назначить встречу.',
    'Хочет поговорить с учителем, чтобы понять программу, лучше назначить встречу.',
    'Хочет поговорить с учителем.', 'Хочет поговорить с учителем, лучше назначить встречу.', 'Хочет поговорить с учителем, чтобы понять программу, лучше назначить встречу.'];

  final List<String> actionHistory = <String>['Редактировал профиль', 'Перенес в Записан на пробный урок', 'Создал заявку', 'Создал заявку', 'Создал заявку'];

  final commentText = TextEditingController();

  int clientId;
  String clientSurname;
  String clientName;
  String clientMiddleName;
  String clientPhone;
  String clientCourse;
  String clientSource;
  String clientCity;
  String clientHasLaptop;
  String clientStatus;

  @override
  void initState() {
    context.read<LeadDetailsBloc>().add(LoadLeadDetails(clientId));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Map arguments = ModalRoute.of(context).settings.arguments as Map;

    clientId = arguments[AppConstants.CLIENT_ID];
    clientSurname = arguments[AppConstants.CLIENT_SURNAME];
    clientName = arguments[AppConstants.CLIENT_NAME];
    clientPhone = arguments[AppConstants.CLIENT_PHONE];
    clientCity = arguments[AppConstants.CLIENT_CITY];
    clientCourse = arguments[AppConstants.CLIENT_COURSE];
    clientStatus = arguments[AppConstants.CLIENT_STATUS];

    //clientSource = arguments[AppConstants.CLIENT_SOURCE];
    //clientCity = arguments[AppConstants.CLIENT_CITY];
    //clientHasLaptop = arguments[AppConstants.CLIENT_HAS_LAPTOP];

    print(arguments[AppConstants.CLIENT_ID]);
    print(arguments[AppConstants.CLIENT_SURNAME]);
    print(arguments[AppConstants.CLIENT_NAME]);
    print(arguments[AppConstants.CLIENT_PHONE]);
    print(arguments[AppConstants.CLIENT_CITY]);
    print(arguments[AppConstants.CLIENT_COURSE]);
    //print(arguments[AppConstants.CLIENT_SOURCE]);
    //print(arguments[AppConstants.CLIENT_HAS_LAPTOP]);
    print(arguments[AppConstants.CLIENT_STATUS]);

    return Scaffold(
          appBar: _getAppBar(context),
          backgroundColor: ColorPalettes.white,
          body: SingleChildScrollView(
              padding: const EdgeInsets.symmetric(horizontal: 18),
              //color:  ColorPalettes.boardViewListColor,
              child: Column(
                children: [
                  clientCard(context),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        AppLocalizations.of(context).translate('comments:'),
                        style: TextStyle(
                            fontFamily: 'Golos',
                            color: ColorPalettes.textColorBlack,
                            fontSize: 15,
                            fontWeight: FontWeight.w500),
                      ),
                    ],
                  ),
                  SizedBox(height: 18),
                  Card(
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(6),
                      side: BorderSide(width: 1, color: ColorPalettes.greyStroke),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 16, right: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          for (int item = 0; item < date.length; item++)
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 14,),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      date[item],
                                      style: TextStyle(
                                          height: 1.5,
                                          fontFamily: 'Golos',
                                          color: ColorPalettes.textColorGrey,
                                          fontSize: 13,
                                          fontWeight: FontWeight.w400),
                                    ),
                                    Text(
                                      ' ',
                                      style: TextStyle(
                                          height: 1.5,
                                          fontFamily: 'Golos',
                                          color: ColorPalettes.textColorGrey,
                                          fontSize: 13,
                                          fontWeight: FontWeight.w400),
                                    ),
                                    Text(
                                      time[item],
                                      style: TextStyle(
                                          height: 1.5,
                                          fontFamily: 'Golos',
                                          color: ColorPalettes.textColorGrey,
                                          fontSize: 13,
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 7,),
                                Text(
                                  commentedName[item],
                                  style: TextStyle(
                                      height: 1.5,
                                      fontFamily: 'Golos',
                                      color: ColorPalettes.textColorGrey,
                                      fontSize: 13,
                                      fontWeight: FontWeight.w800),
                                ),
                                SizedBox(height: 7,),
                                Text(
                                  comments[item],
                                  style: TextStyle(
                                      height: 1.5,
                                      fontFamily: 'Golos',
                                      color: ColorPalettes.textColorBlack,
                                      fontSize: 15,
                                      fontWeight: FontWeight.w400),
                                ),
                                SizedBox(height: 14,),
                                Divider(
                                  height: 0,
                                  thickness: 1,
                                  color: ColorPalettes.greyStroke,
                                  indent: 0,
                                  endIndent: 0,
                                ),
                              ],
                            )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 18),
                  CustomButtonWhiteSmall(
                    text: AppLocalizations.of(context).translate('showMore'),
                    isClickable: true,
                    onPressed: () {
                    },
                  ),
                  SizedBox(height: 20),
                  Card(
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(6),
                      side: BorderSide(width: 1, color: ColorPalettes.greyStroke),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            AppLocalizations.of(context).translate('addComment'),
                            style: TextStyle(
                                height: 1.5,
                                fontFamily: 'Golos',
                                color: ColorPalettes.textColorGrey,
                                fontSize: 14,
                                fontWeight: FontWeight.w500),
                          ),
                          SizedBox(height: 7),
                          TextField(
                            onChanged: (text) {
                              setState(() {

                              });
                            },
                            controller: commentText,
                            keyboardType: TextInputType.multiline,
                            maxLines: 5,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.only(left: 18, top: 4, bottom: 4),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(6)),
                                borderSide: BorderSide(
                                  color: ColorPalettes.greyStroke,
                                  width: 1.0,
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(6)),
                                borderSide: BorderSide(
                                  color: ColorPalettes.greyStroke,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 18),
                  CustomButtonWhiteSmall(
                    text: AppLocalizations.of(context).translate('send'),
                    isClickable: commentText.text.length > 0,
                    onPressed: () {
                    },
                  ),
                  SizedBox(height: 40),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        AppLocalizations.of(context).translate('actionHistory'),
                        style: TextStyle(
                            color: ColorPalettes.textColorBlack,
                            fontFamily: 'Golos',
                            fontSize: 15,
                            fontWeight: FontWeight.w500),
                      ),
                    ],
                  ),
                  SizedBox(height: 18),
                  Card(
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(6),
                      side: BorderSide(width: 1, color: ColorPalettes.greyStroke),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 16, right: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          for (int item = 0; item < actionHistory.length; item++)
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 14,),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      date[item],
                                      style: TextStyle(
                                          height: 1.5,
                                          fontFamily: 'Golos',
                                          color: ColorPalettes.textColorGrey,
                                          fontSize: 13,
                                          fontWeight: FontWeight.w400),
                                    ),
                                    Text(
                                     '  time[item]',
                                      style: TextStyle(
                                          height: 1.5,
                                          fontFamily: 'Golos',
                                          color: ColorPalettes.textColorGrey,
                                          fontSize: 13,
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 7,),
                                Text(
                                  commentedName[item],
                                  style: TextStyle(
                                      height: 1.5,
                                      fontFamily: 'Golos',
                                      color: ColorPalettes.textColorGrey,
                                      fontSize: 13,
                                      fontWeight: FontWeight.w800),
                                ),
                                SizedBox(height: 7,),
                                Text(
                                  actionHistory[item],
                                  style: TextStyle(
                                      height: 1.5,
                                      fontFamily: 'Golos',
                                      color: ColorPalettes.textColorBlack,
                                      fontSize: 15,
                                      fontWeight: FontWeight.w400),
                                ),
                                SizedBox(height: 14,),
                                Divider(
                                  height: 0,
                                  thickness: 1,
                                  color: ColorPalettes.greyStroke,
                                  indent: 0,
                                  endIndent: 0,
                                ),
                              ],
                            )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 40),
                  CustomButton(
                    text: AppLocalizations.of(context).translate('toStudentApplication'),
                    isClickable: true,
                      onPressed: () {

                      }
                  ),
                  SizedBox(height: 20),
                  CustomButtonWhite(
                    text: AppLocalizations.of(context).translate('toWaitingList'),
                    isClickable: true,
                    onPressed: () {

                    }
                  ),
                  SizedBox(height: 20),
                ],
              )
      ),
    );
  }

  _getAppBar(BuildContext context) {
    return AppBar(
      elevation: 1,
      backgroundColor: ColorPalettes.white,
      brightness: Brightness.light,
      leading: IconButton(
          icon: SvgPicture.asset(IconAssets.backIcon),
          onPressed: () {
            Navigator.pop(context);
          }),
      titleSpacing: 0.0,
      title: Text(
        AppLocalizations.of(context).translate('clientCard'),
          style: TextStyle(
              color: ColorPalettes.textColorBlack,
              fontFamily: 'Golos',
              fontWeight: FontWeight.w500),
      ),
      actions: <Widget>[
        IconButton(
            icon: SvgPicture.asset(IconAssets.notificationsIconBlack),
            onPressed: () {
              Navigation.intentWithData(context, NotificationsPage.routeName, {
                AppConstants.IS_APPBAR_ALLOWED: true,
              });
            }),
      ],
    );
  }

  Widget clientCard(BuildContext context) {
    return BlocBuilder<LeadDetailsBloc, LeadDetailsState>(
      builder: (context, state) {
        print('State is $state');
        if (state is LeadDetailsHasData) {
          clientId = state.leadDetailsResult.lead.id;
          clientSurname = state.leadDetailsResult.lead.surname;
          clientName = state.leadDetailsResult.lead.name;
          clientMiddleName = state.leadDetailsResult.lead.middleName;
          clientPhone = state.leadDetailsResult.lead.phone;
          clientCourse = state.leadDetailsResult.lead.courseName;
          //clientSource = state.leadDetailsResult.lead.;
          clientCity = state.leadDetailsResult.lead.cityName;
          //clientHasLaptop = state.leadDetailsResult.lead..toString();
          clientStatus = state.leadDetailsResult.lead.leadStatus;
          context.read<LeadDetailsBloc>().add(LeadDetailsStateChange());
        }
        if (state is LeadDetailsNoData) {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            CustomToast.showCustomToast(context, AppLocalizations.of(context).translate('couldNotDownloadData'));
          });
        }
        if (state is LeadDetailsNoInternetConnection) {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            CustomToast.showCustomToast(context, AppLocalizations.of(context).translate('noInternetConnection'));
          });
        }
        if (state is LeadDetailsError) {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            CustomToast.showCustomToast(context, AppLocalizations.of(context).translate('couldNotDownloadData'));
          });
        }
        return Column(
          children: [
            SizedBox(height: 18),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'ID ${clientId??AppLocalizations.of(context).translate('notIndicated')}',
                  style: TextStyle(
                      color: ColorPalettes.textColorGrey,
                      fontFamily: 'Golos',
                      fontSize: 13,
                      fontWeight: FontWeight.w400),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      '01.05.21',
                      style: TextStyle(
                          color: ColorPalettes.textColorGrey,
                          fontFamily: 'Golos',
                          fontSize: 13,
                          fontWeight: FontWeight.w400),
                    ),
                    Text(
                      ' | 18.00',
                      style: TextStyle(
                          color: ColorPalettes.textColorGrey,
                          fontFamily: 'Golos',
                          fontSize: 13,
                          fontWeight: FontWeight.w400),
                    )
                  ],
                ),
              ],
            ),
            SizedBox(height: 14),
            Card(
              elevation: 0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(6),
                side: BorderSide(width: 1, color: ColorPalettes.purpleGreyStroke),
              ),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      '${clientSurname??''} ${clientName??''} ${clientMiddleName??''}',
                      style: TextStyle(
                          height: 1.5,
                          fontFamily: 'Golos',
                          color: ColorPalettes.textColorBlack,
                          fontSize: 15,
                          fontWeight: FontWeight.w500),
                    ),
                    Container(
                        transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                        child: Text(
                          clientPhone??AppLocalizations.of(context).translate('notIndicated'),
                          style: TextStyle(
                              height: 1.5,
                              fontFamily: 'Golos',
                              color: ColorPalettes.textColorGrey,
                              fontSize: 15,
                              fontWeight: FontWeight.w500),
                        )
                    ),
                    Container(
                        transform: Matrix4.translationValues(-5.0, 0.0, 0.0),
                        margin: EdgeInsets.only(top: 5, bottom: 5),
                        child:Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Card(
                              elevation: 0,
                              color: Color(0xFFFDD835),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(6)),
                              child: Padding(
                                  padding: const EdgeInsets.all(6.0),
                                  child: Text(
                                    clientCourse??AppLocalizations.of(context).translate('notIndicated'),
                                    style: TextStyle(
                                        fontFamily: 'Golos',
                                        color: ColorPalettes.white,
                                        fontSize: 13,
                                        fontWeight: FontWeight.w400),
                                  )),
                            ),
                            Card(
                              elevation: 0,
                              color: Color(0xFFFDD835),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(6)),
                              child: Padding(
                                  padding: const EdgeInsets.all(6.0),
                                  child: Text(
                                    clientSource??AppLocalizations.of(context).translate('notIndicated'),
                                    style: TextStyle(
                                        fontFamily: 'Golos',
                                        color: ColorPalettes.white,
                                        fontSize: 13,
                                        fontWeight: FontWeight.w400),
                                  )),
                            )
                          ],
                        )),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          AppLocalizations.of(context).translate('city:'),
                          style: TextStyle(
                              height: 1.5,
                              fontFamily: 'Golos',
                              color: ColorPalettes.textColorBlack,
                              fontSize: 13,
                              fontWeight: FontWeight.w400),
                        ),
                        Text(
                          clientCity??AppLocalizations.of(context).translate('notIndicated'),
                          style: TextStyle(
                              height: 1.5,
                              fontFamily: 'Golos',
                              color: ColorPalettes.textColorBlack,
                              fontSize: 13,
                              fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          AppLocalizations.of(context).translate('laptop:'),
                          style: TextStyle(
                              height: 1.5,
                              fontFamily: 'Golos',
                              color: ColorPalettes.textColorBlack,
                              fontSize: 13,
                              fontWeight: FontWeight.w400),
                        ),
                        Text(
                          clientHasLaptop??AppLocalizations.of(context).translate('notIndicated'),
                          style: TextStyle(
                              height: 1.5,
                              fontFamily: 'Golos',
                              color: ColorPalettes.textColorBlack,
                              fontSize: 13,
                              fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          AppLocalizations.of(context).translate('status:'),
                          style: TextStyle(
                              height: 1.5,
                              fontFamily: 'Golos',
                              color: ColorPalettes.textColorBlack,
                              fontSize: 13,
                              fontWeight: FontWeight.w400),
                        ),
                        Text(
                          clientStatus??AppLocalizations.of(context).translate('notIndicated'),
                          style: TextStyle(
                              height: 1.5,
                              fontFamily: 'Golos',
                              color: ColorPalettes.textColorBlack,
                              fontSize: 13,
                              fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 40)
          ],
        );
      },
    );
  }

}
