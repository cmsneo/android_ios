import 'package:flutter/material.dart';

class RPSCustomPainter extends CustomPainter{

  @override
  void paint(Canvas canvas, Size size) {



    Paint paint_0 = new Paint()
      ..color = Color.fromARGB(255, 33, 150, 243)
      ..style = PaintingStyle.fill
      ..strokeWidth = 1;


    Path path_0 = Path();
    path_0.moveTo(size.width,size.height);
    path_0.quadraticBezierTo(size.width*0.2500000,size.height,0,size.height);
    path_0.lineTo(0,size.height*0.7023200);
    path_0.quadraticBezierTo(size.width*0.0818250,size.height*0.7020200,size.width*0.3752125,size.height*0.7013000);
    path_0.cubicTo(size.width*0.3748000,size.height*0.9629600,size.width*0.6237500,size.height*0.9516800,size.width*0.6252125,size.height*0.7010000);
    path_0.quadraticBezierTo(size.width*0.8459250,size.height*0.7001600,size.width,size.height*0.7000000);
    path_0.quadraticBezierTo(size.width*0.9981250,size.height*0.7675000,size.width,size.height);
    path_0.close();

    canvas.drawPath(path_0, paint_0);


  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }

}
