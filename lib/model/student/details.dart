import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'details.g.dart';

@JsonSerializable()
class StudentDetailsResult extends Equatable {

  @JsonKey(name: 'status')
  final int status;

  @JsonKey(name: 'message')
  final String message;

  @JsonKey(name: 'responseObj')
  final Student student;

  const StudentDetailsResult(this.status, this.message, this.student);

  @override
  List<Object> get props => [status, message, student];

  factory StudentDetailsResult.fromJson(Map<String, dynamic> json) => _$StudentDetailsResultFromJson(json);

  Map<String, dynamic> toJson() => _$StudentDetailsResultToJson(this);
}

@JsonSerializable()
class Student extends Equatable {
  @JsonKey(name: 'id')
  final int id;

  @JsonKey(name: 'surname')
  final String surname;

  @JsonKey(name: 'name')
  final String name;

  @JsonKey(name: 'middleName')
  final String middleName;

  @JsonKey(name: 'cityName')
  final String cityName;

  @JsonKey(name: 'phone')
  final String phone;

  @JsonKey(name: 'email')
  final String email;

  @JsonKey(name: 'address')
  final String address;

  @JsonKey(name: 'hasLaptop')
  final bool hasLaptop;

  @JsonKey(name: 'discriminator')
  final String discriminator;

  @JsonKey(name: 'groups')
  final List<Group> groups;

  Student(this.id, this.surname, this.name, this.middleName, this.cityName, this.phone, this.email, this.address, this.hasLaptop, this.discriminator, [this.groups = const []]);

  @override
  List<Object> get props => [
    id,
    surname,
    name,
    middleName,
    cityName,
    phone,
    email,
    address,
    hasLaptop,
    discriminator
  ];

  factory Student.fromJson(Map<String, dynamic> json) => _$StudentFromJson(json);

  Map<String, dynamic> toJson() => _$StudentToJson(this);
}

@JsonSerializable()
class Group extends Equatable {
  @JsonKey(name: 'id')
  final int id;

  @JsonKey(name: 'name')
  final String name;

  @JsonKey(name: 'cityName')
  final String cityName;

  @JsonKey(name: 'months')
  final int months;

  Group(this.id, this.name, this.cityName, this.months);

  @override
  List<Object> get props => [
    id,
    name,
    cityName,
    months
  ];

  factory Group.fromJson(Map<String, dynamic> json) => _$GroupFromJson(json);

  Map<String, dynamic> toJson() => _$GroupToJson(this);
}