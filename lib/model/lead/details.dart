import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'details.g.dart';

@JsonSerializable()
class LeadDetailsResult extends Equatable {

  @JsonKey(name: 'status')
  final int status;

  @JsonKey(name: 'message')
  final String message;

  @JsonKey(name: 'responseObj')
  final Lead lead;

  const LeadDetailsResult(this.status, this.message, this.lead);

  @override
  List<Object> get props => [status, message, lead];

  factory LeadDetailsResult.fromJson(Map<String, dynamic> json) => _$LeadDetailsResultFromJson(json);

  Map<String, dynamic> toJson() => _$LeadDetailsResultToJson(this);
}

@JsonSerializable()
class Lead extends Equatable {
  @JsonKey(name: 'id')
  final int id;

  @JsonKey(name: 'surname')
  final String surname;

  @JsonKey(name: 'name')
  final String name;

  @JsonKey(name: 'middleName')
  final String middleName;

  @JsonKey(name: 'cityName')
  final String cityName;

  @JsonKey(name: 'phone')
  final String phone;

  @JsonKey(name: 'courseName')
  final String courseName;

  @JsonKey(name: 'leadStatus')
  final String leadStatus;

  @JsonKey(name: 'flexById')
  final String flexById;

  @JsonKey(name: 'leadFailureStatusId')
  final int leadFailureStatusId;

  Lead(this.id, this.surname, this.name, this.middleName, this.cityName, this.phone, this.courseName, this.leadStatus, this.flexById, this.leadFailureStatusId);

  @override
  List<Object> get props => [
    id,
    surname,
    name,
    middleName,
    cityName,
    phone,
    courseName,
    leadStatus,
    flexById,
    leadFailureStatusId
  ];

  factory Lead.fromJson(Map<String, dynamic> json) => _$LeadFromJson(json);

  Map<String, dynamic> toJson() => _$LeadToJson(this);
}