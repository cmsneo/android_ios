// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'details.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LeadDetailsResult _$LeadDetailsResultFromJson(Map<String, dynamic> json) {
  return LeadDetailsResult(
    json['status'] as int,
    json['message'] as String,
    json['responseObj'] == null
        ? null
        : Lead.fromJson(json['responseObj'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$LeadDetailsResultToJson(LeadDetailsResult instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'responseObj': instance.lead,
    };

Lead _$LeadFromJson(Map<String, dynamic> json) {
  return Lead(
    json['id'] as int,
    json['surname'] as String,
    json['name'] as String,
    json['middleName'] as String,
    json['cityName'] as String,
    json['phone'] as String,
    json['courseName'] as String,
    json['leadStatus'] as String,
    json['flexById'] as String,
    json['leadFailureStatusId'] as int,
  );
}

Map<String, dynamic> _$LeadToJson(Lead instance) => <String, dynamic>{
      'id': instance.id,
      'surname': instance.surname,
      'name': instance.name,
      'middleName': instance.middleName,
      'cityName': instance.cityName,
      'phone': instance.phone,
      'courseName': instance.courseName,
      'leadStatus': instance.leadStatus,
      'flexById': instance.flexById,
      'leadFailureStatusId': instance.leadFailureStatusId,
    };
