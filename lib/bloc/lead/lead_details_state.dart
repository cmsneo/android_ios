import 'package:android_ios/model/lead/details.dart';
import 'package:equatable/equatable.dart';

abstract class LeadDetailsState extends Equatable {
  const LeadDetailsState();

  @override
  List<Object> get props => [];
}

class InitialLeadDetails extends LeadDetailsState {}

class LeadDetailsLoading extends LeadDetailsState {}

class LeadDetailsHasData extends LeadDetailsState {

  final LeadDetailsResult leadDetailsResult;

  const LeadDetailsHasData([this.leadDetailsResult]);

  @override
  List<Object> get props => [leadDetailsResult];
}

class LeadDetailsNoData extends LeadDetailsState {
  final String message;

  const LeadDetailsNoData(this.message);

  @override
  List<Object> get props => [message];
}

class LeadDetailsNoInternetConnection extends LeadDetailsState {}

class LeadDetailsError extends LeadDetailsState {
  final String errorMessage;

  const LeadDetailsError(this.errorMessage);

  @override
  List<Object> get props => [errorMessage];
}
