import 'package:equatable/equatable.dart';

abstract class LeadDetailsEvent extends Equatable {
  final int leadId;

  const LeadDetailsEvent(this.leadId);

  @override
  List<Object> get props => [leadId];
}

class LoadLeadDetails extends LeadDetailsEvent {
  LoadLeadDetails(int leadId) : super(leadId);
}

class LeadDetailsStateChange extends LeadDetailsEvent {
  LeadDetailsStateChange([int leadId]) : super(leadId);
}


