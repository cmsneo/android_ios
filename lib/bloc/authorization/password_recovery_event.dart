import 'package:equatable/equatable.dart';

abstract class PasswordRecoveryEvent extends Equatable {
  final String email;

  const PasswordRecoveryEvent(this.email);

  @override
  List<Object> get props => [email];
}

class GenerateNewPassword extends PasswordRecoveryEvent {
  GenerateNewPassword(String email) : super(email);
}

class PasswordRecoveryStateChange extends PasswordRecoveryEvent {
  PasswordRecoveryStateChange([String email]) : super(email);
}